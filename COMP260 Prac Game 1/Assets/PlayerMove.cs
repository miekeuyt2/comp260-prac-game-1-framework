﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    private BeeSpawnerSimple beeSpawner;
    private void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawnerSimple>();
    }
    public float maxSpeed = 5.0f;
    public GameObject p1;
    public GameObject p2;
    public float destroyRadius = 1.0f;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }


        if (gameObject.name.Equals("Player"))
        {
            Vector2 direction1;
            direction1.x = Input.GetAxis("Horizontal");
            direction1.y = Input.GetAxis("Vertical");
            // scale by the maxSpeed parameter
            Vector2 velocity1 = direction1 * maxSpeed;

            // move the object
            transform.Translate(velocity1 * Time.deltaTime);
        }
        else 
        {
            Vector2 direction2;
            direction2.x = Input.GetAxis("Horizontal2");
            direction2.y = Input.GetAxis("Vertical2");
            // scale by the maxSpeed parameter
            Vector2 velocity2 = direction2 * maxSpeed;

            // move the object
            transform.Translate(velocity2 * Time.deltaTime);
        }
        // get the input values
       /* Vector2 direction;
        direction.x = Input.GetAxis("Horizontal");
        direction.y = Input.GetAxis("Vertical");
        
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);*/
    }
}
