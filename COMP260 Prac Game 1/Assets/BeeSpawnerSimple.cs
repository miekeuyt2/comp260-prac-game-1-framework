﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawnerSimple : MonoBehaviour
{

    public BeeMoveSImple beePrefab;
    public PlayerMove player1;
    public PlayerMove player2;
    int nBees = 50;
    public Rect spawnRect;
    public ParticleSystem explosionPrefab;
    int beePeriodSec;
    int minBeePeriod = 0;
    int maxBeePeriod = 5;
    Boolean spawning = false;
    int bees = 0;

    IEnumerator SpawnObject(int sec, int idx)
    {
        yield return new WaitForSeconds(sec);
        //for (int i = 0; i < nBees; i++)
        //{
            // instantiate a bee
            BeeMoveSImple bee = Instantiate(beePrefab);
            bee.explosionPrefab = explosionPrefab;
            bee.Target = player1.transform;
            bee.target1 = player1.transform;
            bee.target2 = player2.transform;
            // attach to this object in the hierarchy
            bee.transform.parent = transform;
            // give the bee a name and number
            bee.gameObject.name = "Bee " + idx;
        idx++;

            float x = spawnRect.xMin +
               UnityEngine.Random.value * spawnRect.width;
            float y = spawnRect.yMin +
                UnityEngine.Random.value * spawnRect.height;

            bee.transform.position = new Vector2(x, y);
            beePeriodSec = UnityEngine.Random.Range(minBeePeriod, maxBeePeriod);
            spawning = false;

        //}
    }

    void Start()
    {
        beePeriodSec = UnityEngine.Random.Range(minBeePeriod, maxBeePeriod);
        
    }
    private void Update()
    { 
            if (spawning == false)
            {
                spawning = true;
                StartCoroutine(SpawnObject(beePeriodSec, bees));
                bees++;

            }
    }

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }

    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // Fixed bug by adding a type conversion
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }


}
