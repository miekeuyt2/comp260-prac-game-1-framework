﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMoveSImple : MonoBehaviour {
    public float speed = 4.0f;        // metres per second
    public float turnSpeed = 180.0f;  // degrees per second
    public Transform Target;
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;
    public ParticleSystem explosionPrefab;
    void Update()
    {
        // get the vector from the bee to the target 
        Vector2 direction1 = target1.position - transform.position;

        Vector2 direction2 = target2.position - transform.position;
        Vector2 direction = Target.position - transform.position;
        if (direction1.magnitude > direction2.magnitude)
        {
            Target = target2;
        }
        else
        {
            Target = target1;
        }
        //Vector2 direction = target.position - transform.position;

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = Target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);

    }

}
