﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour
{
    public float minSpeed = 0;
    public float maxSpeed = 10;
    public float minTurnSpeed = 0;
    public float maxTurnSpeed = 5;
    public ParticleSystem explosionPrefab;


    // private state
    private float speed = 5;
    private float turnSpeed = 5;

    public Transform target1;
    public Transform target2;
    public Transform Target;
    
    private Vector2 heading = Vector2.right;




    private void Start()
    {
/*
        heading = Vector2.right;
        float angle = Random.value * 360;
        heading = heading.Rotate(angle);

        // set speed and turnSpeed randomly 
        speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, Random.value);
        */
    }
    
    void Update()
    {

        // get the vector from the bee to the target 
        
        Vector2 direction1 = target1.position - transform.position;

        Vector2 direction2 = target2.position - transform.position;
        Vector2 direction = Target.position - transform.position;
        if (direction1.magnitude > direction2.magnitude)
        {
            Target = target2;
        }
        else
        {
            Target = target1;
        }

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (direction.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }
    void OnDrawGizmos()
    {
        // draw heading vector in red
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, heading);

        // draw target vector in yellow
        Gizmos.color = Color.yellow;
        Vector2 direction = Target.position - transform.position;
        Gizmos.DrawRay(transform.position, direction);
    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        // destroy the particle system when it is over
        Destroy(explosion.gameObject, explosion.duration);

    }

}
